# frozen_string_literal: true

RSpec.shared_examples "paginated endpoint" do |args|
  it "returns paginated response" do
    expect(status).to eq(200)
    expect_paginated_response(total: args[:total], total_pages: args[:total_pages])
  end
end
