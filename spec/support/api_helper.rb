# frozen_string_literal: true

RSpec.shared_context("with api helper") do
  let(:user) { create(:user) }
  let(:with_basic_auth) { false }
  let(:basic_auth) { "#{user.username}:#{user.password}" }

  def api_get(uri, body = {})
    get(uri, params: json(body), headers: headers(nil))
  end

  def api_post(uri, body = {}, gitlab_token = nil)
    post(uri, params: json(body), headers: headers(gitlab_token))
  end

  def api_put(uri, body = {}, gitlab_token = nil)
    put(uri, params: json(body), headers: headers(gitlab_token))
  end

  def api_delete(uri)
    delete(uri, headers: headers(nil))
  end

  def json(body)
    body.is_a?(Hash) ? body : hash(body)
  end

  def hash(body)
    JSON.parse(File.read(body), symbolize_names: true)
  end

  def expect_status(status)
    expect(response.status).to eq(status)
  end

  def expect_json(json)
    expect(JSON.parse(response.body, symbolize_names: true)).to eq(json)
  end

  def expect_entity_response(entity, object)
    expect(response.body).to eq(entity.represent(object).to_json)
  end

  def expect_paginated_response(total:, total_pages:)
    expect(response.headers.to_h).to include(
      "X-Total" => total.to_s,
      "X-Total-Pages" => total_pages.to_s,
      "X-Per-Page" => "25",
      "X-Page" => "1",
      "X-Offset" => "0"
    )
  end

  private

  def headers(token)
    {
      "content_type" => "application/json",
      # add authorization header only if basic auth is being tested
      "Authorization" => with_basic_auth ? "Basic #{Base64.encode64(basic_auth)}" : nil,
      "X-Gitlab-Token" => token
    }.compact_blank
  end
end
