# frozen_string_literal: true

# Load metrics only on web containers
return if Sidekiq.server? && !AppConfig.metrics?

ENV["YABEDA_SIDEKIQ_COLLECT_CLUSTER_METRICS"] = "true"

require "yabeda/sidekiq"
require "yabeda/prometheus"

Yabeda.configure do
  group :vulnerabilities do
    gauge :issue_count,
          comment: "Number of active vulnerability issues",
          tags: %i[project],
          aggregation: :most_recent
    gauge :merge_request_count,
          comment: "Number of open merge requests fixing vulnerability",
          tags: %i[project],
          aggregation: :most_recent
    gauge :total_count,
          comment: "Total number of vulnerabilities detected",
          tags: %i[project],
          aggregation: :most_recent
  end

  collect do
    Project.each do |project|
      label = { project: project.name }

      vulnerabilities.issue_count.set(label, MetricsHelper.active_vulnerability_issues(project))
      vulnerabilities.total_count.set(label, MetricsHelper.total_vulnerabilities(project))
      vulnerabilities.merge_request_count.set(label, MetricsHelper.active_vulnerability_merge_requests(project))
    end
  end
end
