# frozen_string_literal: true

class SessionsController < ApplicationController
  before_action :authenticate_user!, only: [:destroy]

  # Render sign in page
  #
  # @return [void]
  def new
    redirect_if_authenticated
  end

  # Create new user sessions
  #
  # @return [void]
  def create
    user = find_user(username: params[:username])

    unless user.present? && user.authenticate(params[:password])
      return redirect_to(sign_in_path, alert: "Invalid username or password!")
    end

    login(user)
    remember(user) if params[:remember_me] == "1"
    redirect_to projects_path
  end

  # Log out
  #
  # @return [void]
  def destroy
    logout
    forget(current_user)
    redirect_to sign_in_path
  end
end
